from projects.views import project_list, show_project, create_project
from django.urls import path

# from tasks.views import show_project


urlpatterns = [
    path("<int:id>/", show_project, name="show_project"),
    path("", project_list, name="list_projects"),
    path("create/", create_project, name="create_project"),
]
